# Praktikum Pemrograman Dasar

## Modul C++
1. [Pengantar C++](./pengantar.md)
2. [Hello World](./hello-world.md)
3. [Memahami Struktur Kode Dasar](./struktur-kode-dasar.md)
4. [**Variabel** : Tempat Penyimpanan Nilai Data](./variable.md)
5. [Tipe Data](./tipe-data.md)
5. [**String** : Kumpulan Karakter](./string.md)
6. [**Input dan Output**](./input-output.md)
7. [Operator](./operator.md)
8. [**Boolean** : Benar atau Salah ?](./boolean.md)
9. [**Condition** : Jika A Maka B](./condition.md)
10. [**Switch** : Penanganan Berbagai Nilai Kondisi](./switch.md)
11. [**While** : Ulang Eksekusi Kode Jika Kondisi Terpenuhi](./.md)
12. [**For** : Ulang Eksekusi Kode N Kali](./.md)
13. [**Break & Continue** : Sudahi atau Lewati Pengulangan](./.md)
14. [**Array** : Kumpulan Data Dalam Satu Variabel](./.md)
15. [**Function** : Membungkus Sekumpulan Perintah](./.md)
16. [**Return** : Output dari Function](./.md)
17. [**Struct** : Wadah Variabel-Variabel Saling Terkait](./.md)
18. [Class dan Object](./.md)
19. [**Attribute** - Objeknya Seperti Apa ?](./.md)
20. [**Method** - Objeknya Bisa Apa ?](./.md)
21. [Constructor](./.md)
22. [Siapa yang Boleh Akses ? - Modifier](./.md)
23. [Inheritance](./.md)
24. [Reference dan Pointer](./.md)
25. [Baca Tulis File](./.md)
26. [User Interface Berbasis Console](./.md)

## Pertemuan
1. Perkenalan, Aturan Perkuliahan, OBE, Penilaian, Perkenalan Tools, set akun Git, nonton video game console, siapin naskah game
2. Import / Header, Variable, Data Type, String, Input Output, nonton video
3. Operator, Bitwise, dan Boolean
4. Condition dan Switch
5. Iterasi - For, While, Break, Continue
6. Array
7. Function, Input Type, Return Type
8. UTS
9. Struct
10. Class, Object, Method, Attribute
11. Constructor, Modifier
12. Inheritance
13. Reference dan Pointer
14. Akses Baca Tulis File
15. User Interface Berbasis Console
16. UAS
